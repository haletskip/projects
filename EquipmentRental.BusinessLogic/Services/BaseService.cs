﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.DataLayer.Entities;
using EquipmentRental.DataLayer.Interfaces;

namespace EquipmentRental.BusinessLogic.Services
{
    public abstract class BaseService<TModel, TEntity>
        where TModel : BaseDto
        where TEntity : BaseEntity
    {
        protected readonly IContextFactory _contextFactory;
        protected readonly IMapper _mapper;

        public BaseService(IContextFactory contextFactory, IMapper mapper)
        {
            _contextFactory = contextFactory;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TModel>> GetAllAsync()
        {
            using (var context = _contextFactory.Create())
            {
                var entities = context.Set<TEntity>();

                return _mapper.Map<IEnumerable<TEntity>, IEnumerable<TModel>>(await entities.ToListAsync());
            }
        }

        public async Task<TModel> GetAsync(string id)
        {
            using (var context = _contextFactory.Create())
            {
                var entity = context.Set<TEntity>();

                return _mapper.Map<TEntity, TModel>(await entity.FirstOrDefaultAsync(x => x.Id == id));
            }
        }
    }
}
