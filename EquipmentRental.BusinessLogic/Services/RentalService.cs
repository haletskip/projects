﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Enums;
using EquipmentRental.BusinessLogic.Helpers;
using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.BusinessLogic.Models;
using EquipmentRental.DataLayer.Entities;
using EquipmentRental.DataLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace EquipmentRental.BusinessLogic.Services
{
    public class RentalService : BaseService<RentalDto, Rental>, IRentalService
    {
        private readonly IDocumentBuilder _documentBuilder;

        public RentalService(IContextFactory contextFactory, IMapper mapper,
            IDocumentBuilder documentBuilder)
            : base(contextFactory, mapper)
        {
            _documentBuilder = documentBuilder;
        }

        public async Task<string> CreateAsync(IEnumerable<RentalDetailDto> rentalDetailDtos, string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (rentalDetailDtos.Any() != true)
            {
                throw new ArgumentNullException(nameof(rentalDetailDtos));
            }

            using (var context = _contextFactory.Create())
            {
                var user = await context.Users.FirstOrDefaultAsync(x => x.Email == email);
                var rental = new Rental
                {
                    StartDate = DateTime.Now,
                    User = user
                };

                rental.RentalDetails = rentalDetailDtos.Select(x =>
                {
                    var equipment = context.Equipments.FirstOrDefault(y => y.Id == x.EquipmentId);

                    return new RentalDetail
                    {
                        Equipment = equipment,
                        Rental = rental,
                        RentalDays = x.RentalDays
                    };
                }).ToList();

                context.Rentals.Add(rental);

                await context.SaveChangesAsync();

                return rental.Id;
            }
        }

        public async Task<byte[]> GetInvoiceAsync(string email)
        {
            var invoice = await GetRentalInvoiceAsync(email);

            return _documentBuilder.BuildRentalInvoice(invoice);
        }

        public decimal GetPrice(EquipmentTypes equipmentType, int rentalDays)
        {
            return PriceFactory.Create(equipmentType, rentalDays).CalculateTotal();
        }

        public async Task<IEnumerable<RentalDto>> GetRentalsForUserAsync(string email)
        {
            using (var context = _contextFactory.Create())
            {
                var userRentals = await context.Rentals.Where(x => x.User.Email == email)?.ToListAsync();

                return _mapper.Map<List<Rental>, IEnumerable<RentalDto>>(userRentals);
            }
        }

        private async Task<RentalInvoice> GetRentalInvoiceAsync(string email)
        {
            if (email == null)
            {
                throw new ArgumentNullException(nameof(email));
            }

            var invoiceItems = new List<List<string>>
            {
                new List<string>
                {
                    "#",
                    "Equipment Name",
                    "Equipment Type",
                    "Rental Days",
                    "Bonus Points",
                    "Price"
                }
            };

            var bonusPointsAmount = 0;
            decimal totalPrice = 0;

            using (var context = _contextFactory.Create())
            {
                var userRentals = await context.Rentals.Where(x => x.User.Email == email)?.ToListAsync();
                var userRentalDetails = userRentals.SelectMany(userRental => userRental.RentalDetails)?.ToList();

                for (int i = 0; i < userRentalDetails.Count; i++)
                {
                    var equipmentType = (EquipmentTypes)userRentalDetails[i].Equipment?.EquipmentType;
                    var rentalDays = userRentalDetails[i].RentalDays;
                    var name = userRentalDetails[i].Equipment?.Name;
                    var bonusPoints = equipmentType == EquipmentTypes.Heavy ? 2 : 1;
                    var price = PriceFactory.Create(equipmentType, rentalDays).CalculateTotal();
                    var index = i + 1;

                    bonusPointsAmount += bonusPoints;
                    totalPrice += price;

                    var items = new List<object>
                    {
                        index,
                        name,
                        equipmentType,
                        rentalDays,
                        bonusPoints,
                        price
                    };

                    invoiceItems.Add(items.Select(Convert.ToString)?.ToList());
                }
            }

            return new RentalInvoice
            {
                InvoiceItems = invoiceItems,
                TotalBonusPoints = bonusPointsAmount,
                TotalPrice = totalPrice
            };
        }
    }
}