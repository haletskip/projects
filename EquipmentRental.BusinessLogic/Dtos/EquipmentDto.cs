﻿using EquipmentRental.BusinessLogic.Enums;

namespace EquipmentRental.BusinessLogic.Dtos
{
    public class EquipmentDto : BaseDto
    {
        public string Name { get; set; }

        public string ImgSource { get; set; }

        public EquipmentTypes EquipmentType { get; set; }
    }
}
