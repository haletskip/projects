﻿using EquipmentRental.BusinessLogic.Models;

namespace EquipmentRental.BusinessLogic.Interfaces
{
    public interface IDocumentBuilder
    {
        byte[] BuildRentalInvoice(RentalInvoice rentalInvoice);
    }
}
