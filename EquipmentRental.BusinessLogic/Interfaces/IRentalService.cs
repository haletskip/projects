﻿using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EquipmentRental.BusinessLogic.Interfaces
{
    public interface IRentalService
    {
        Task<byte[]> GetInvoiceAsync(string email);

        Task<IEnumerable<RentalDto>> GetRentalsForUserAsync(string email);

        decimal GetPrice(EquipmentTypes equipmentType, int rentalDays);

        Task<string> CreateAsync(IEnumerable<RentalDetailDto> rentalDetailDtos, string email);
    }
}
