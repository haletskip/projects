﻿using EquipmentRental.BusinessLogic.Interfaces;
using EquipmentRental.BusinessLogic.Services;
using EquipmentRental.DataLayer;
using Unity;
using Unity.Extension;

namespace EquipmentRental.BusinessLogic.Utilities
{
    public class BusinessLogicDependencyInjection : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.AddNewExtension<DataLayerDependencyInjection>();
            Container.RegisterType<IEquipmentService, EquipmentService>();
            Container.RegisterType<IRentalService, RentalService>();
            Container.RegisterType<IDocumentBuilder, DocumentBuilder>();
        }
    }
}
