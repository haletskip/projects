﻿using System.Collections.Generic;

namespace EquipmentRental.BusinessLogic.Models
{
    public class RentalInvoice
    {
        public decimal TotalPrice { get; set; }

        public decimal TotalBonusPoints { get; set; }

        public List<List<string>> InvoiceItems { get; set; } = new List<List<string>>();
    }
}
