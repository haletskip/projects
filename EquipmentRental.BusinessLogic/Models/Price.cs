﻿using EquipmentRental.BusinessLogic.Constants;

namespace EquipmentRental.BusinessLogic.Models
{
    public abstract class Price
    {
        protected int _rentalDays;

        public Price(int rentalDays)
        {
            _rentalDays = rentalDays;
        }

        public abstract decimal CalculateTotal();

        protected decimal CalculateExtrasPrice(int rentalDays, int extraDays)
        {
            decimal price = 0;

            if (rentalDays <= extraDays)
            {
                price += FeeConstants.PremiumFee * rentalDays;
            }
            else if (rentalDays > extraDays)
            {
                var daysDiffer = rentalDays - extraDays;
                var premiumFeeAmount = FeeConstants.PremiumFee * extraDays;
                var regularFeeAmount = FeeConstants.RegularFee * daysDiffer;

                price += premiumFeeAmount + regularFeeAmount;
            }

            return price;
        }
    }
}