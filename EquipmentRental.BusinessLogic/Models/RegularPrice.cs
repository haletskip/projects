﻿using EquipmentRental.BusinessLogic.Constants;

namespace EquipmentRental.BusinessLogic.Models
{
    public sealed class RegularPrice : Price
    {
        public RegularPrice(int rentalDays)
            : base(rentalDays)
        {
            _rentalDays = rentalDays;
        }

        public override decimal CalculateTotal()
        {
            return FeeConstants.OneTimeFee + CalculateExtrasPrice(_rentalDays, 2);
        }
    }
}