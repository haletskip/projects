﻿using EquipmentRental.BusinessLogic.Constants;

namespace EquipmentRental.BusinessLogic.Models
{
    public sealed class HeavyPrice : Price
    {
        public HeavyPrice(int rentalDays)
            : base(rentalDays)
        {
        }

        public override decimal CalculateTotal()
        {
            var premiumFeeAmount = FeeConstants.PremiumFee * _rentalDays;

            return FeeConstants.OneTimeFee + premiumFeeAmount;
        }
    }
}