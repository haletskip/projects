﻿using EquipmentRental.BusinessLogic.Models;
using EquipmentRental.BusinessLogic.Enums;
using System;

namespace EquipmentRental.BusinessLogic.Helpers
{
    public static class PriceFactory
    {
        public static Price Create(EquipmentTypes equipmentType, int rentalDays)
        {
            switch (equipmentType)
            {
                case EquipmentTypes.Heavy:
                    return new HeavyPrice(rentalDays);

                case EquipmentTypes.Regular:
                    return new RegularPrice(rentalDays);

                case EquipmentTypes.Specialized:
                    return new SpecializedPrice(rentalDays);

                default:
                    throw new ArgumentException($"Can not calculate a price with specified {nameof(equipmentType)} equipment type");
            }
        }
    }
}