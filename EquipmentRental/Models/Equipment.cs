﻿using EquipmentRental.Enums;
using System.Collections.Generic;

namespace EquipmentRental.Models
{
    public class Equipment : BaseModel
    {
        public string Name { get; set; }

        public string ImgSource { get; set; }

        public EquipmentTypes EquipmentType { get; set; }
    }
}
