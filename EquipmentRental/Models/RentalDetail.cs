﻿namespace EquipmentRental.Models
{
    public class RentalDetail : BaseModel
    {
        public string RentalId { get; set; }

        public string EquipmentId { get; set; }

        public int RentalDays { get; set; }

        public virtual Equipment Equipment { get; set; }
    }
}
