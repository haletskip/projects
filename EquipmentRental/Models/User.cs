﻿using System.Collections.Generic;

namespace EquipmentRental.Models
{
    public class User : BaseModel
    {
        public string Name { get; set; }

        // TODO: add hashed password
        // public string Password { get; set; }

        public string Email { get; set; }

        public ICollection<Rental> Rentals { get; set; } = new HashSet<Rental>();
    }
}
