var equipmentrental = window.equipmentrental || {};
equipmentrental.generic = equipmentrental.generic || {};

equipmentrental.generic = (function () {

    function init() {
        $(document)
            .off('submit')
            .on('submit', ".ajax-form", function (event) {
                event.preventDefault();

                var form = $(this);
                var dataToPost = new FormData(form.get(0));
                var elementId = form.data('elementId');
                var onSuccess = form.data("onSuccess");

                $.validator.unobtrusive.parse(form); 
                var isFormValid = form.valid();

                if (isFormValid) {
                    $.ajax({
                        url: form.attr("action"),
                        method: "POST",
                        data: dataToPost,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            if (data.isSuccess) {
                                var callback = eval(onSuccess);

                                if (typeof callback === 'function') {
                                    callback(data);
                                }
                            } else {
                                $(elementId).replaceWith(data);
                                equipmentrental.common.subscriber.subsribe();
                            }
                        }
                    });
                }
            });
    }

    return {
        init: init
    };

})();

equipmentrental.generic.callbacks = (function () {

    function onSuccessAddRentalDays(data) {
        $('#equipmentModal').modal('hide');

        $.get(data.redirectUrl, function (response) {
            var count = $(response).data("basket-items-count");

            $('#shoppingBasketItems').text(count);
        });
    }

    return {
        onSuccessAddRentalDays: onSuccessAddRentalDays
    };
})();