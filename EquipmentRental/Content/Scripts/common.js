var equipmentrental = window.equipmentrental || {};
equipmentrental.common = equipmentrental.common || {};

equipmentrental.common.subscriber = (function () {

    function subscribe() {
        equipmentrental.basket.init();
        equipmentrental.generic.init();
    }

    return {
        subscribe: subscribe
    };
})();