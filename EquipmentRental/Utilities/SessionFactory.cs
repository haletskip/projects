﻿using EquipmentRental.Interfaces;
using System.Web;

namespace EquipmentRental.Utilities
{
    public class SessionFactory : ISessionFactory
    {
        public T GetItem<T>(string key)
            where T : new()
        {
            if (HttpContext.Current.Session[key] is T model)
            {
                return model;
            }

            model = new T();
            HttpContext.Current.Session[key] = model;

            return model;
        }

        public void Clear()
        {
            HttpContext.Current.Session.Clear();
        }
    }
}