﻿using AutoMapper;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.Models;
using EquipmentRental.ViewModels;

namespace EquipmentRental.Utilities
{
    public class AutoMapperWebProfile : Profile
    {
        public AutoMapperWebProfile()
        {
            CreateMap<BaseDto, BaseModel>().ReverseMap();
            CreateMap<EquipmentDto, Equipment>().ReverseMap();
            CreateMap<RentalDto, Rental>().ReverseMap();
            CreateMap<RentalDetailDto, RentalDetail>().ReverseMap();
            CreateMap<RentalDetailDto, BasketItem>().ReverseMap();
        }
    }
}