﻿﻿﻿using EquipmentRental.Interfaces;
using EquipmentRental.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace EquipmentRental.Controllers
{
    public class SharedController : Controller
    {
        private readonly ISessionFactory _sessionFactory;

        public SharedController(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        [ChildActionOnly]
        public ActionResult Navigation(string selectedItem)
        {
            if (string.IsNullOrEmpty(selectedItem))
            {
                throw new ArgumentNullException(nameof(selectedItem));
            }

            var model = GetNavigationBarViewModel(selectedItem);

            return PartialView("_Navigation", model);
        }

        private NavigationBarViewModel GetNavigationBarViewModel(string selectedItem)
        {
            var menuItems = new List<MenuItem>
            {
                new MenuItem
                {
                    Value = "Equipments",
                    Text = "Equipments",
                    NavigateUrl = Url.Action("Index", "Equipment")
                },
                new MenuItem
                {
                    Value = "Rentals",
                    Text = "Rentals",
                    NavigateUrl = Url.Action("Index", "Rental")
                },
            };

            var selectedMenuItem = menuItems.FirstOrDefault(x => x.Text == selectedItem);

            if (selectedMenuItem != null)
            {
                selectedMenuItem.Selected = true;
            }

            var model = new NavigationBarViewModel
            {
                MenuItems = menuItems,
                BasketItemsCount = _sessionFactory.GetItem<BasketViewModel>("Basket").Count
            };

            return model;
        }
    }
}