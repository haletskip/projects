﻿using EquipmentRental.BusinessLogic.Interfaces;
using System.Web.Mvc;
using System.Net.Mime;
using EquipmentRental.Constants;
using System;
using System.Threading.Tasks;
using AutoMapper;
using System.Collections.Generic;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.Models;

namespace EquipmentRental.Controllers
{
    public class RentalController : Controller
    {
        private readonly IRentalService _rentalService;
        private readonly IMapper _mapper;

        public RentalController(IMapper mapper, IRentalService rentalService)
        {
            _mapper = mapper;
            _rentalService = rentalService;
        }

        public async Task<ActionResult> Invoice()
        {
            var invoice = await _rentalService.GetInvoiceAsync(FakeUserInfo.Email);

            if (invoice == null)
            {
                throw new NullReferenceException(nameof(invoice));
            }

            return File(invoice, MediaTypeNames.Application.Octet, "Invoice.docx");
        }

        public async Task<ActionResult> Index()
        {
            var rentalsDto = await _rentalService.GetRentalsForUserAsync(FakeUserInfo.Email);
            var rentals = _mapper.Map<IEnumerable<RentalDto>, IEnumerable<Rental>>(rentalsDto);

            return View(rentals);
        }
    }
}