﻿using Common.Interfaces;
using System;
using Unity;
using System.Web.Mvc;
using System.Web.Routing;
using System.Configuration;

namespace EquipmentRental.Attributes
{
    public class ExceptionFilterAttribute : FilterAttribute, IExceptionFilter
    {
        private const string ErrorUrl = "/Error";

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            var errorUrl = ConfigurationManager.AppSettings["ErrorUrl"] ?? ErrorUrl;
            var logger = UnityConfig.Container.Resolve<ILogger>();

            filterContext.ExceptionHandled = true;
            filterContext.Result = new RedirectResult(errorUrl);
           
            logger.Error(GenerateMessage(filterContext.Exception, filterContext.RouteData));
        }

        private string GenerateMessage(Exception exception, RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var methodName = exception.TargetSite;

            return $"MethodName: <<{methodName}>> Controller: <<{controllerName}>> Action: <<{actionName}>>";
        }
    }
}