﻿namespace EquipmentRental.Enums
{
    public enum EquipmentTypes
    {
        Heavy,
        Regular,
        Specialized
    }
}