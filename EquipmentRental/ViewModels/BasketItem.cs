﻿using EquipmentRental.Models;

namespace EquipmentRental.ViewModels
{
    public class BasketItem
    {
       public Equipment Equipment { get; set; }

       public int RentalDays { get; set; }

       public decimal Price { get; set; }

       public int Quantity { get; set; }
    }
}