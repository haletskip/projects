﻿using EquipmentRental.BusinessLogic.Interfaces;
using Unity;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using System.Threading.Tasks;
using EquipmentRental.BusinessLogic.Dtos;
using EquipmentRental.BusinessLogic.Enums;
using EquipmentRental.Models;

namespace EquipmentRental.ViewModels
{
    public class BasketViewModel
    {
        private readonly IEquipmentService _equipmentService;
        private readonly IRentalService _rentalService;
        private readonly IMapper _mapper;

        public BasketViewModel()
        {
            _mapper = UnityConfig.Container.Resolve<IMapper>();
            _rentalService = UnityConfig.Container.Resolve<IRentalService>();
            _equipmentService = UnityConfig.Container.Resolve<IEquipmentService>();
        }

        public async Task<BasketItem> AddAsync(string equipmentId, int rentalDays)
        {
            var equipmentDto = await _equipmentService.GetAsync(equipmentId);
            var equipment = _mapper.Map<EquipmentDto, Equipment>(equipmentDto);
            var basketItem = BasketItems.FirstOrDefault(x => x.Equipment.Id == equipmentId);

            if (basketItem == null)
            {
                basketItem = new BasketItem
                {
                    Equipment = equipment,
                    Quantity = 1,
                    RentalDays = rentalDays,
                    Price = _rentalService.GetPrice((EquipmentTypes)equipment.EquipmentType, rentalDays)
                };

                BasketItems.Add(basketItem);
            }
            else
            {
                basketItem.Quantity++;
            }

            return basketItem;
        }

        public decimal? Amount => BasketItems?.Sum(x => x.Price * x.Quantity);

        public int Count => BasketItems.Sum(x => x.Quantity);

        public List<BasketItem> BasketItems { get; set; } = new List<BasketItem>();
    }
}