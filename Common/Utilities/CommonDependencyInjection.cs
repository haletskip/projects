﻿using Common;
using Common.Interfaces;
using Unity;
using Unity.Extension;
using Unity.Lifetime;

namespace EquipmentRental.BusinessLogic.Utilities
{
    public class CommonDependencyInjection : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Container.RegisterType<ILogger, Logger>(new ContainerControlledLifetimeManager());
        }
    }
}
