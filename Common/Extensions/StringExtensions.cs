﻿using System;

namespace Common.Extensions
{
    public static class StringExtensions
    {
        public static bool StringEquals(this string a, string b)
        {
            return string.Equals(a, b, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
