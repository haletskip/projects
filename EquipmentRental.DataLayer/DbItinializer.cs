﻿using System.Linq;
using System.Data.Entity;
using EquipmentRental.DataLayer.Entities;
using System.Collections.Generic;
using EquipmentRental.DataLayer.Enums;
using System;

namespace EquipmentRental.DataLayer
{
    public class DbInitializer : DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {
            var equipment1 = new Equipment
            {
                Name = "Caterpillar bulldozer",
                ImgSource = "/Content/Images/Equipments/Caterpillar_Bulldozer.png",
                EquipmentType = EquipmentTypes.Heavy
            };

            var equipment2 = new Equipment
            {
                Name = "KamAZ truck",
                ImgSource = "/Content/Images/Equipments/KamAZ_Truck.jpg",
                EquipmentType = EquipmentTypes.Regular
            };

            var equipment3 = new Equipment
            {
                Name = "Komatsu crane",
                ImgSource = "/Content/Images/Equipments/Komatsu_Crane.jpg",
                EquipmentType = EquipmentTypes.Heavy
            };

            if (!context.Equipments.Any())
            {
                var equipmentList = new List<Equipment>
                {
                    equipment1,
                    equipment1,
                    equipment1,
                    new Equipment
                    {
                        Name = "Volvo steamroller",
                        ImgSource = "/Content/Images/Equipments/Volvo_Steamroller.jpg",
                        EquipmentType = EquipmentTypes.Heavy
                    },
                    new Equipment
                    {
                        Name = "Bosch jackhammer",
                        ImgSource = "/Content/Images/Equipments/Bosch_Jackhammer.jpg",
                        EquipmentType = EquipmentTypes.Specialized
                    }
                };

                context.Equipments.AddRange(equipmentList);
                context.SaveChanges();
            }

            var user = new User
            {
                Name = "TestUser",
                Email = "test@test.com"
            };

            if (!context.Users.Any())
            {

                context.Users.Add(user);
                context.SaveChanges();
            }

            var rental = new Rental
            {
                StartDate = DateTime.Now,
                User = user
            };

            rental.RentalDetails = new List<RentalDetail>
                {
                    new RentalDetail
                    {
                        Equipment = equipment1,
                        Rental = rental,
                        RentalDays = 15
                    },
                    new RentalDetail
                    {
                        Equipment = equipment2,
                        Rental = rental,
                        RentalDays = 3
                    },
                    new RentalDetail
                    {
                        Equipment = equipment3,
                        Rental = rental,
                        RentalDays = 2
                    },
                };

            if (!context.Rentals.Any())
            {
                context.Rentals.Add(rental);
                context.SaveChanges();
            }
        }
    }
}
