﻿namespace EquipmentRental.DataLayer.Enums
{
    public enum EquipmentTypes
    {
        Heavy,
        Regular,
        Specialized
    }
}
