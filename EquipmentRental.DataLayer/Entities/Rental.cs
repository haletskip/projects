﻿using System;
using System.Collections.Generic;

namespace EquipmentRental.DataLayer.Entities
{
    public class Rental : BaseEntity
    {
        public DateTime StartDate { get; set; }

        public string UserId { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<RentalDetail> RentalDetails { get; set; } = new HashSet<RentalDetail>();
    }
}
