﻿using System;

namespace EquipmentRental.DataLayer.Entities
{
    public abstract class BaseEntity
    {
        private string _id;

        public string Id {
            get
            {
                if (string.IsNullOrEmpty(_id))
                {
                    _id = Guid.NewGuid().ToString();
                }

                return _id;
            }
            set
            {
                _id = value;
            }
        }
    }
}
