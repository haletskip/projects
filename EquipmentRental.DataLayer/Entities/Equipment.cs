﻿using EquipmentRental.DataLayer.Enums;
using System.Collections;
using System.Collections.Generic;

namespace EquipmentRental.DataLayer.Entities
{
    public class Equipment : BaseEntity
    {
        public string Name { get; set; }

        public string ImgSource { get; set; }

        public EquipmentTypes EquipmentType { get; set; }

        public virtual ICollection<RentalDetail> RentalDetails { get; set; } = new HashSet<RentalDetail>();
    }
}
