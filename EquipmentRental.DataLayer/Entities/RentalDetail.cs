﻿namespace EquipmentRental.DataLayer.Entities
{
    public class RentalDetail : BaseEntity
    {
        public string RentalId { get; set; }

        public string EquipmentId { get; set; }

        public int RentalDays { get; set; }

        public virtual Rental Rental { get; set; }

        public virtual Equipment Equipment { get; set; }
    }
}
