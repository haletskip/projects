﻿using EquipmentRental.DataLayer.Entities;
using System.Data.Entity;

namespace EquipmentRental.DataLayer
{
    public class Context : DbContext
    {
        public Context(string connectionString)
            : base(connectionString)
        {
            Database.SetInitializer(new DbInitializer());
        }

        public virtual DbSet<Equipment> Equipments { get; set; }

        public virtual DbSet<Rental> Rentals { get; set; }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<RentalDetail> RentalDetails { get; set; }
    }
}
