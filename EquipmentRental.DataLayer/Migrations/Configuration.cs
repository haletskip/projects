using System.Data.Entity.Migrations;

namespace EquipmentRental.DataLayer.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }
    }
}
